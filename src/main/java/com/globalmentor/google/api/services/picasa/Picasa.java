/*
 * Copyright © 2011 GlobalMentor, Inc. <http://www.globalmentor.com/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.globalmentor.google.api.services.picasa;

import static java.util.Objects.*;

/**
 * Constants and utilities for accessing Google Picasa.
 * 
 * @author Garret Wilson
 * 
 * @see <a href="http://picasa.google.com/">Picasa</a>
 * @see <a href="http://code.google.com/apis/gdata/faq.html#clientlogin">Google ClientLogin Service Names</a>
 */
public class Picasa {

	/** The Picasa Web Albums Data API service name. */
	public static final String SERVICE_NAME = "lh2";

	/** The base path for the Picasa API URI, relative to <code>/data/</code>. */
	public static final String DATA_API_BASE_PATH = "feed/api/";

	/** The URI path segment for indicating a user. */
	public static final String DATA_API_USER_PATH_SEGMENT = "user";

	/** The URI path segment for indicating an album ID. */
	public static final String DATA_API_ALBUM_ID_PATH_SEGMENT = "albumid";

	/**
	 * Creates a URI path relative to <code>/data/</code> for listing all albums of a user.
	 * @param username The Picasa username.
	 * @return The URI path relative to <code>/data/</code> for listing all albums of the given user.
	 * @throws NullPointerException if the given username is <code>null</code>.
	 */
	public static String createDataAPIUserPath(final String username) {
		return DATA_API_BASE_PATH + DATA_API_USER_PATH_SEGMENT + '/' + requireNonNull(username);
	}

	/**
	 * Creates a URI path relative to <code>/data/</code> for listing all pictures in a user's album.
	 * @param username The Picasa username.
	 * @param albumID The ID of the user's album.
	 * @return The URI path relative to <code>/data/</code> for listing all pictures in the given user's indicated album.
	 * @throws NullPointerException if the given username and/or album ID is <code>null</code>.
	 */
	public static String createDataAPIUserAlbumPath(final String username, final String albumID) {
		return createDataAPIUserPath(username) + '/' + DATA_API_ALBUM_ID_PATH_SEGMENT + '/' + requireNonNull(albumID);
	}

}
